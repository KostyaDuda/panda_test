<?php

class TestModel extends Model
{
	

    public function get_data_()
	{	
		return $this->data_base->get_all_by_param_data_db('user','id',2);
	}

	public function set_data_()
	{	
		$data = [
			'name' => 'Test',
			'password' => '4321',
			'status' => 2
		];

		$this->data_base->insert_data_to_db($data,'user');
	}

	public function update_data(){
		$data = [
			'id' => 5,
			'name' => 'Test',
			'password' => '4321',
			'status' => 2
		];

		$this->data_base->update_data_to_db($data,'user');
	}

	public function delete_data(){
		$data = 'id = 5';

		$this->data_base->delete_by_param('user',$data);
	}
}