<?php 

class Post extends Model{

    public $title;
	public $status;
	public $created;
    public $user_id;

    public $db;

    

    function __construct()
	{
        $this->db = new DataBaseModel();
		$find_user = $this->db->get_one_by_param_data_db('user','username',$_SESSION['username']);
        $this->user_id = $find_user['id'];
	
	}

    public function get_posts()
	{	
        return $this->db->get_all_by_param_data_db('posts','user_id',$this->user_id);
	}

    public function get_one_post($id)
	{	
        return $this->db->get_one_by_param_data_db('posts','id',$id);
	}

	    /**
     *
     *  @param mix $data
	 *  @return array
     *
     */
    public function validation_post($data)
	{	
		$errors = [];
		
		if(empty($data['title'])){
			array_push($errors,"Введіть назву опитування");
		}
		if(empty($data['status'])){
			array_push($errors,'Статус немає бути пустий');
		}

		if(empty($errors)){
			return ['success_validation' => 1,
					'message' => 'Форму успішно відправлено!'];
		}
		else{
			return [
				'success_validation' => 0,
			 	'errors' => $errors,
				'data' => $data
			];
		}
	}

    /**
     *
     * @param mix $data
     *
     */
    public function set_post($data)
	{	
		$post = [
			'title' => $data['title'],
			'status' => $data['status'],
			'created' => date("Y-m-d H:i:s", time()),
            'user_id' => $this->user_id
		];

		$this->db->insert_data_to_db($post,'posts');
	}

    public function update_post($post,$data)
	{	
		$post = [
            'id' => $data['id'],
			'title' => $post['title'],
			'status' => $post['status'],
			'created' => $data['created'],
            'user_id' => $data['user_id']
		];

		$this->db->update_data_to_db($post,'posts');

	}

    public function delete_post($id)
	{	
		$data_post = 'id= '.$id;
		$data_answers = 'id_post= '.$id;
		$this->db->delete_by_param('answers', $data_answers);  
		$this->db->delete_by_param('posts', $data_post);    
	}

}