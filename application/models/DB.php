<?php

class DataBaseModel{
    
    function __construct()
	{
        DB::$user = 'root';
        DB::$password = '';
        DB::$dbName = 'panda_test';
	}

    public function insert_data_to_db($value,$table){
        DB::insert($table, $value);
    }

    public function update_data_to_db($value,$table){
        DB::replace($table, $value);
    }

    public function get_all_by_param_data_db($table,$param,$value){

        return DB::query("SELECT * FROM ".$table." WHERE ". $param." = ". $value);
    }

    public function get_one_by_param_data_db($table,$param,$value){
        return DB::queryFirstRow("SELECT * FROM ".$table." WHERE ".$param."=%s",$value);
    }

    public function get_count_by_param_data_db($table,$param,$value){
        return DB::queryFirstField("SELECT COUNT(*) FROM ".$table." WHERE ". $param." = ". $value);
    }

    public function delete_by_param($table,$value){
        DB::delete($table, $value);
    }
}