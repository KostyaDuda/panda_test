<?php 

class Answer extends Model{

    public $answer;
	public $id_post;
	public $count_voiсes;
    public $status;

    public $db;

    

    function __construct()
	{
        $this->db = new DataBaseModel();
	
	}

    public function get_answers($id)
	{	
        return $this->db->get_all_by_param_data_db('answers','id_post',$id);
	}

    public function get_one_answers($id)
	{	
        return $this->db->get_one_by_param_data_db('answers','id',$id);
	}

	    /**
     *
     *  @param mix $data
	 *  @return array
     *
     */
    public function validation_answer($data)
	{	
		$errors = [];
		
		if(empty($data['answer'])){
			array_push($errors,"Введіть назву відповіді");
		}
		if(empty($data['status'])){
			array_push($errors,'Статус немає бути пустий');
		}
        if(empty($data['count_voices'])){
			array_push($errors,'Кількість голосів не має бути пустим');
		}
        if($data['count_voices'] < 0){
			array_push($errors,'Кількість голосів не може бути мінусовим');
		}

		if(empty($errors)){
			return ['success_validation' => 1,
					'message' => 'Форму успішно відправлено!'];
		}
		else{
			return [
				'success_validation' => 0,
			 	'errors' => $errors,
				'data' => $data
			];
		}
	}

    /**
     *
     * @param mix $data
     *
     */
    public function set_answer($data,$id_post)
	{	
		$answer = [
			'id_post' => $id_post,
			'answer' => $data['answer'],
			'count_voices' => $data['count_voices'],
            'status' => $data['status']
		];

		$this->db->insert_data_to_db($answer,'answers');
	}

    public function update_answer($post,$data)
	{	
		
        $answer = [
            'id' => $data['id'],
			'id_post' => $data['id_post'],
			'answer' => $post['answer'],
			'count_voices' => $post['count_voices'],
            'status' => $post['status']
		];

		$this->db->update_data_to_db($answer,'answers');

	}

    public function delete_answers($id)
	{	
		$data = 'id= '.$id;
		$this->db->delete_by_param('answers', $data);    
	}

}