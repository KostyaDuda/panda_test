<?php

class User extends Model{


	public $username;
	public $status;
	public $password;

    public function get_data()
	{	
	}

    /**
     *
     * @param mix $data
     *
     */
    public function set_user($data)
	{	
		$user = [
			'username' => $data['name'],
			'password' => $this->encryptPassword($data['password']),
			'status' => 1
		];

		$this->data_base->insert_data_to_db($user,'user');
	}

	
    /**
     *
     *  @param mix $data
	 *  @return array
     *
     */
    public function validation($data)
	{	
		$errors = [];
		$db = new DataBaseModel();
		
		if(empty($data['name'])){
			array_push($errors,"Введіть ім`я");
		}
		if(!empty($data['name']) && !empty($db->get_one_by_param_data_db('user','username',$data['name']))){
			array_push($errors,"Даний логін вже зареєстований");
		}
		if(empty($data['password'])){
			array_push($errors,'Введіть пароль');
		}
		if(empty($data['again_password'])){
			array_push($errors,'Введіть пароль ще раз');
		}
		if(!empty($data['password']) && !empty($data['again_password']) && $data['password'] != $data['again_password']){
			array_push($errors,'Паролі не співпадають!');
		}

		if(empty($errors)){
			return ['success_validation' => 1,
					'message' => 'Форму успішно відправлено!'];
		}
		else{
			return [
				'success_validation' => 0,
			 	'errors' => $errors,
				'data' => $data
			];
		}
	}

	/**
     *
     *  @param mix $data
	 *  @return array
     *
     */
    public function get_user($data){
		$db = new DataBaseModel();
		$query = $db->get_one_by_param_data_db('user','username',$data['name']);
		return $query;
	}

	public function validate_login($data){
		$errors = [];
		$db = new DataBaseModel();

		if(!empty($data['name']) && !empty($data['password'])){
			$user = $db->get_one_by_param_data_db('user','username',$data['name']);

			if(!empty($user)){

				if(!$this->verifyPassword($data['password'], $user['password'])){
					array_push($errors,"Не вірний логін або пароль");		
				}
			}
			else{
				array_push($errors,"Не вірний логін або пароль");	
			}
		}
		else{	
			array_push($errors,'Заповніть всі поля');
		}
		
		if(empty($errors)){
			return [
				'success_login' => true,
					'message' => 'Форму успішно відправлено!',
					'user' => $user];
		}
		else{
			return [
				'success_login' => false,
			 	'errors' => $errors,
				'data' => $data
			];
		}
	}

	/**
     *
     *  @param array $data
	 *  @return array
     *
     */
    public function set_user_session($data){
	
		$_SESSION['username'] = $data['username'];

		return $_SESSION['username'] ;
	}

	public function get_user_session(){
		return $_SESSION['username'];
	}

	public function verify_login_user(){
		return isset($_SESSION['username']);
	}

	public function logout_user(){

		unset($_SESSION['username']);

	}

	function encryptPassword($password) {
		$hashedPassword = password_hash($password, PASSWORD_BCRYPT);
		return $hashedPassword;
	}

	function verifyPassword($password, $hashedPassword) {
		return password_verify($password, $hashedPassword);
	}
}