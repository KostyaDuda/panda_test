<?php

class Route
{

	static function start()
    {
        $controller_name = 'Main';
        $action_name = 'index';

        $user_auth_access = [
            'controller_main' => true,
            'controller_signup'=> true,
            'controller_login' => true,
            'controller_posts' => false,
            'controller_answers' => false,
            'controller_api' => true
        ];
        
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }
        
        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        $model_name = 'Model_'.$controller_name;
        $controller_name = 'Controller_'.$controller_name;
        $action_name = 'action_'.$action_name;

        $model_file = strtolower($model_name).'.php';
        $model_path = "application/models/".$model_file;
        if (file_exists($model_path)) {
            include "application/models/".$model_file;
        }

        $controller_file = strtolower($controller_name).'.php';
        $controller_path = "application/controllers/".$controller_file;
        if (file_exists($controller_path)) {
            include "application/controllers/".$controller_file;
        } else {
            Route::ErrorPage404();
        }
        
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            if (!$user_auth_access[strtolower($controller_name)]) {
                if (isset($_SESSION['username'])) {
                    $params = array_slice($routes, 3);


                    $controller->$action(...$params);
                } else {
                    Route::ErrorPage403();
                }
            } else {
                
                $controller->$action();
            }
        } else {
            Route::ErrorPage404();
        }
    }
	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
		
    }

	function ErrorPage403()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 403');
		header("Status: 403");
		header('Location:'.$host.'403');
    }
    
}
