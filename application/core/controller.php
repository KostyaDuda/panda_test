<?php

class Controller {
	
	public $model;
	public $view;
	public $user;
	
	function __construct()
	{
		$this->view = new View();
	}

	function render_auth_menu(){
        $template_view = '';
        if(isset($_SESSION['username'])){
            $template_view = 'template_auth_view.php';
        }
        else{
            $template_view = 'template_view.php';
        }
        return $template_view;
    }
	
	function action_index()
	{
		// todo	
	}

}
