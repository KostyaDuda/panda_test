<title>Оновити відповідь</title>
<div class="container">
<h2>Оновити відповідь</h2>
    <form method="POST" action="/answers/update/<?php echo $data['id']; ?>">
    <?php if(isset($data['errors'])): ?>
    <div class="error-message"><?php foreach ($data['errors'] as $error)
           echo '<div class="alert alert-danger mt-3" role="alert">
                   '.$error.'
                 </div>';
        ?></div>
    <?php endif; ?>

    <?php if(isset($data['message'])): ?>
        <div class="alert alert-success mt-3" role="alert">
          <?php echo $data['message']; ?>
        </div>
    <?php endif; ?>
      <div class="form-group">
          <label for="surveyTitle">Текст відповіді</label>
          <input type="text" class="form-control" name="answer" id="surveyTitle" value="<?php echo $data['answer'] ?>" placeholder="Введіть назву опитування">
      </div>
      <div class="form-group">
          <label for="surveyTitle">Кількість голосів</label>
          <input type="number" class="form-control" name="count_voices" id="surveyTitle" value="<?php echo $data['count_voices'] ?>" placeholder="Введіть кількість голосів">
      </div>
      <div class="form-group">
        <label for="surveyStatus">Статус</label>
        <select class="form-control" name="status" id="surveyStatus">
          <option value="1" <?php echo ($data['status'] == 1) ? 'selected' : ''; ?>>Чернетка</option>
          <option value="2" <?php echo ($data['status'] == 2) ? 'selected' : ''; ?>>Публікація</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Створити</button>
    </form>
</div>