<title>Реєстрація</title>
<div class="container">
    <h2>Реєстрація</h2>
    <form method="POST" action="/signup/store">
    <?php if(isset($data['errors'])): ?>
    <div class="error-message"><?php foreach ($data['errors'] as $error)
           echo '<div class="alert alert-danger mt-3" role="alert">
                   '.$error.'
                 </div>';
        ?></div>
    <?php endif; ?>

    <?php if(isset($data['message'])): ?>
        <div class="alert alert-success mt-3" role="alert">
          <?php echo $data['message']; ?>
        </div>
    <?php endif; ?>
      <div class="form-group">
        <label for="loginName">Ім'я</label>
        <input type="text" class="form-control" name="name" id="loginName" placeholder="Введіть ім'я" value="<?php echo (isset($data['data']['name'])) ? $data['data']['name'] : ''; ?>">
      </div>
      <div class="form-group">
        <label for="loginPassword">Пароль</label>
        <input type="password" class="form-control" name="password" id="loginPassword" value="<?php echo (isset($data['data']['password'])) ? $data['data']['password'] : ''; ?>" placeholder="Введіть пароль">
      </div>
      <div class="form-group">
        <label for="confirmPassword">Повторіть пароль</label>
        <input type="password" class="form-control" name="again_password" id="confirmPassword" value="<?php echo (isset($data['data']['again_password'])) ? $data['data']['again_password'] : ''; ?>" placeholder="Повторіть пароль">
      </div>
      <button type="submit" class="btn btn-primary">Зареєструватися</button>
    </form>
  </div>
