<title>Перегляд опитування</title>
<div class="container">
<div class="survey-details">
      <h2><?php echo $data['title']; ?></h2>
      <p><?php echo $data['created']; ?></p>
      
    </div>
    <a href="/answers/create/<?php echo $data['id']?>" class="btn btn-primary">+</a>
    <h3>Варіанти відповідей</h3>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Текст відповіді</th>
          <th scope="col">Статус</th>
          <th scope="col">Кількість голосів</th>
          <th scope="col">Редагування</th>
          <th scope="col">Видалення</th>
        </tr>
      </thead>
      <tbody>
      <?php

      foreach($data['answers'] as $row)
      {
        echo '<tr>
          <td>'.$row['id'].'</td>
          <td>'.$row['answer'].'</td>
          <td>'.get_name_status($row['status']).'</td>
          <td>'.$row['count_voices'].'</td>
          <td><a href="/answers/edit/'.$row['id'].'"><button type="button" class="btn btn-warning"><i class="bi bi-pencil"></i></button></a></td>
          <td>
          <form action="/answers/delete/'.$row['id'].'" method="POST">
            <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i></button>
          </form>
          </td>
        </tr>';
}

?>
      </tbody>
    </table>

    
  </div>

<?php
  function get_name_status($status){
    if($status == 1) return 'Чернетка';
    else return 'Публіковано';
  }
?>