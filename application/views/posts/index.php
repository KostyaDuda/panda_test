<title>Мої опитування</title>
<div class="container">
  <h2>Мої опитування</h2>
    <a href="posts/create"><button type="button" class="btn btn-info btn-circle btn-xl"><i class="bi bi-plus"></i>
    </button></a>
    <table id="sortTable" class="table">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Заголовок опитування</th>
          <th scope="col">Статус</th>
          <th scope="col">Дата створення</th>
          <th scope="col">Редагування</th>
          <th scope="col">Перегляд</th>
          <th scope="col">Видалення</th>
        </tr>
      </thead>
      <tbody>
      <?php

        foreach($data as $row)
        {
            echo '<tr>
            <td>'.$row['id'].'</td>
            <td>'.$row['title'].'</td>
            <td>'.get_name_status($row['status']).'</td>
            <td>'.$row['created'].'</td>
            <td><a href="/posts/edit/'.$row['id'].'"><button type="button" class="btn btn-warning"><i class="bi bi-pencil"></i></button></a></td>
            <td><a href="/posts/show/'.$row['id'].'"><button type="button" class="btn btn-info"><i class="bi bi-eye"></i></button></a></td>
            <td>
            <form action="/posts/delete/'.$row['id'].'" method="POST">
            <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i></button>
            </form>
            </td>
            </tr>';
        }

      ?>
      </tbody>
    </table>
</div>

<?php
  function get_name_status($status){
    if($status == 1) return 'Чернетка';
    else return 'Публіковано';
  }
?>
