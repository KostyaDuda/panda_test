<title>Створення опитування</title>
<div class="container">
<h2>Створення опитування</h2>
    <form method="POST" action="/posts/store">
    <?php if(isset($data['errors'])): ?>
    <div class="error-message"><?php foreach ($data['errors'] as $error)
           echo '<div class="alert alert-danger mt-3" role="alert">
                   '.$error.'
                 </div>';
        ?></div>
    <?php endif; ?>

    <?php if(isset($data['message'])): ?>
        <div class="alert alert-success mt-3" role="alert">
          <?php echo $data['message']; ?>
        </div>
    <?php endif; ?>
      <div class="form-group">
          <label for="surveyTitle">Назва опитування</label>
          <input type="text" class="form-control" name="title" id="surveyTitle" placeholder="Введіть назву опитування">
      </div>
      <div class="form-group">
        <label for="surveyStatus">Статус</label>
        <select class="form-control" name="status" id="surveyStatus">
          <option value="1">Чернетка</option>
          <option value="2">Публікація</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Створити</button>
    </form>
</div>