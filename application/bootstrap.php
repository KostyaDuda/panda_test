<?php

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';


require_once 'models/TestModel.php';
require_once 'models/DB.php';
require_once 'models/User.php';
require_once 'models/Post.php';
require_once 'models/Answer.php';

require_once 'core/route.php';
session_start();
Route::start();

