<?php
class Controller_Answers extends Controller
{
    function __construct()
	{
        $this->view = new View();
		$this->user = new User();
		$this->model = new Answer();
	}

    function action_create($id_post)
	{	
		$this->view->generate('answers/create.php', 'template_auth_view.php',$id_post);
	}


	function action_store($id_post)
	{	
		$data = $this->model->validation_answer($_POST);

		if($data['success_validation'] == 1){
			$this->model->set_answer($_POST,$id_post);
			$this->view->generate('answers/create.php', 'template_auth_view.php', $data);
		}
		elseif($data['success_validation'] == 0){
			$this->view->generate('answers/create.php', 'template_auth_view.php', $data);
		}
	}

	function action_edit($id)
	{	

		$data = $this->model->get_one_answers($id);
		$this->view->generate('answers/update.php', 'template_auth_view.php',$data);
	}

	function action_update($id)
	{	
		$post = $this->model->get_one_answers($id);
		
		$data = $this->model->validation_answer($_POST,$post);

		if($data['success_validation'] == 1){

			$this->model->update_answer($_POST,$post);
			$data_to_post = [
                'id' => $post['id'],
			    'id_post' => $post['id_post'],
			    'answer' => $_POST['answer'],
			    'count_voices' => $_POST['count_voices'],
                'status' => $_POST['status'],
                'message' => $data['message'],
				'errors' => null,
			];
			
			$this->view->generate('answers/update.php', 'template_auth_view.php', $data_to_post);
		}
		elseif($data['success_validation'] == 0){
			$data_to_post = [
                'id' => $post['id'],
			    'id_post' => $post['id_post'],
			    'answer' => $_POST['answer'],
			    'count_voiсes' => $_POST['count_voiсes'],
                'status' => $_POST['status'],
                'message' => null,
				'errors' => $data['errors'],
			];
			$this->view->generate('answers/update.php', 'template_auth_view.php', $data_to_post);
		}
	}

	function action_delete($id)
	{	
		$this->model->delete_answers($id);
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/posts');
        exit;
	}


}