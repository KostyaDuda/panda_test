<?php

class Controller_Main extends Controller
{

    function __construct()
	{
        $this->view = new View();
		$this->model = new TestModel();
		$this->user = new User();
	}

	function action_index()
	{	
		$this->view->generate('main_view.php', $this->render_auth_menu());
	}
	

	function action_logout()
	{	
		$this->user->logout_user();
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
		exit;
	}
}