<?php

class Controller_Posts extends Controller
{

    function __construct()
	{
        $this->view = new View();
		$this->user = new User();
		$this->model = new Post();
	}

	function action_index()
	{	
		$data = $this->model->get_posts();
        $this->view->generate('posts/index.php', 'template_auth_view.php',$data);
	}
	
	function action_create()
	{	
		$this->view->generate('posts/create.php', 'template_auth_view.php');
	}


	function action_store()
	{	
		$data = $this->model->validation_post($_POST);

		if($data['success_validation'] == 1){
			$this->model->set_post($_POST);
			$this->view->generate('posts/create.php', 'template_auth_view.php', $data);
		}
		elseif($data['success_validation'] == 0){
			$this->view->generate('posts/create.php', 'template_auth_view.php', $data);
		}
	}

	function action_edit($id)
	{	
		$data = $this->model->get_one_post($id);
		$this->view->generate('posts/update.php', 'template_auth_view.php',$data);
	}

	function action_update($id)
	{	

		$post = $this->model->get_one_post($id);
		
		$data = $this->model->validation_post($_POST,$post);

		if($data['success_validation'] == 1){

			$this->model->update_post($_POST,$post);
			$data_to_post = [
				'id' => $post['id'],
				'title' => $_POST['title'],
				'status' => $_POST['status'],
				'message' => $data['message'],
				'errors' => null,
			];
			
			$this->view->generate('posts/update.php', 'template_auth_view.php', $data_to_post);
		}
		elseif($data['success_validation'] == 0){
			$data_to_post = [
				'id' => $post['id'],
				'title' => $post['title'],
				'status' => $post['status'],
				'message' => null,
				'errors' => $data['errors'],
			];
			$this->view->generate('posts/update.php', 'template_auth_view.php', $data_to_post);
		}
	}

	function action_show($id)
	{	
		$post_data = $this->model->get_one_post($id);
		$answer = new Answer();
		$answers = $answer->get_answers($id);
		
		$data = [
			'id' => $post_data['id'],
			'title' => $post_data['title'],
			'status' => $post_data['status'],
			'created' => $post_data['created'],
			'answers' => $answers
		];

		$this->view->generate('posts/show.php', 'template_auth_view.php',$data);
	}

	function action_delete($id)
	{	
		$this->model->delete_post($id);
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/posts');
        exit;
	}
}