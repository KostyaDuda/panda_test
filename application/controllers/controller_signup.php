<?php

class Controller_Signup extends Controller
{

    function __construct()
	{
        $this->view = new View();
		$this->model = new User();
	}

	function action_index()
	{	
		$this->view->generate('sign_up/sign_up.php', $this->render_auth_menu());
	}

	function action_store()
	{	
		$data = $this->model->validation($_POST);
		if($data['success_validation'] == 1){
			$this->model->set_user($_POST);
			$this->view->generate('sign_up/sign_up.php', $this->render_auth_menu(), $data);
		}
		elseif($data['success_validation'] == 0){
			$this->view->generate('sign_up/sign_up.php', $this->render_auth_menu(), $data);
		}
	}
}