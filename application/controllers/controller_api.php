<?php

class Controller_Api extends Controller
{
    public $user;
    public $posts;
    public $answers;

    function __construct()
	{
        $this->view = new View();
		$this->user = new User();
	}

	function action_index()
	{	
		
	}

    function action_login()
	{	

        $data = $this->user->validate_login($_POST);

		if($data['success_login'] == true){
            
            $this->user->set_user_session($data['user']);
            echo "Ви авторизовані";
		}
		elseif($data['success_login'] == false){
			print_r($data['errors']);
		}

        if(isset($_SESSION))echo $_SESSION['username'];
		echo json_encode($_POST);
	}

    function action_get_post()
	{	
        if($this->user->verify_login_user()){
            $post = new Post();
            $posts = $post->get_posts();

            if(!empty($posts)){
                $random_post = $posts[array_rand($posts)];

                $answer = new Answer();
    
                $answers = $answer->get_answers($random_post['id']);
    
                $data = [
                    'post' => $random_post,
                    'answers' => $answers
                ];
    
                echo json_encode($data);
            }
            else{
                echo 'У вас відсутні пости';
            }


        }
        else{
            echo "Ви НЕ авторизовані";
        }
	}

    function action_logout()
	{	
        $this->user->logout_user();
		echo "Сесію закрито";
	}
}