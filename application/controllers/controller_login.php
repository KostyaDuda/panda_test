<?php

class Controller_Login extends Controller
{

    function __construct()
	{
        $this->view = new View();
		$this->model = new User();
	}

	function action_index()
	{	
		$this->view->generate('login/login.php', $this->render_auth_menu());
	}

	function action_store()
	{	

		$data = $this->model->validate_login($_POST);

		if($data['success_login'] == true){
            
            $this->model->set_user_session($data['user']);
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/posts');
            exit;
		}
		elseif($data['success_login'] == false){
			$this->view->generate('login/login.php', $this->render_auth_menu(), $data);
		}
	}


}